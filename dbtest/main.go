package main

import (
	"database/sql"
	"flag"
	"log"
	"strconv"

	_ "github.com/lib/pq"
	"github.com/pressly/goose/v3"
)

func main() {
	// Mendefinisikan flag untuk nomor versi dan arah migrasi
	versionFlag := flag.String("version", "", "Specify the migration version to apply")
	directionFlag := flag.String("direction", "up", "Specify the migration direction: up or down")
	flag.Parse()

	// Konfigurasi koneksi database lokal
	dsn := "user=pgusers password=cokro123 dbname=dbtest sslmode=disable host=localhost port=6111"
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("failed to open database: %v\n", err)
	}
	defer db.Close()

	// Mengatur dialect untuk goose
	if err := goose.SetDialect("postgres"); err != nil {
		log.Fatalf("failed to set dialect: %v\n", err)
	}

	switch *directionFlag {
	case "up":
		if *versionFlag == "" {
			// Menjalankan semua migrasi jika versi tidak diberikan
			if err := goose.Up(db, "db/migrations"); err != nil {
				log.Fatalf("failed to apply migrations: %v\n", err)
			}
			log.Println("All migrations applied successfully!")
		} else {
			// Mengubah string versi menjadi int64
			version, err := strconv.ParseInt(*versionFlag, 10, 64)
			if err != nil {
				log.Fatalf("Invalid version format: %v", err)
			}
			// Menerapkan migrasi sampai versi tertentu
			if err := goose.UpTo(db, "db/migrations", version); err != nil {
				log.Fatalf("failed to apply migrations: %v\n", err)
			}
			log.Printf("Migrations up to version %d applied successfully!\n", version)
		}
	case "down":
		if *versionFlag == "" {
			// Membatalkan semua migrasi jika versi tidak diberikan
			if err := goose.Reset(db, "db/migrations"); err != nil {
				log.Fatalf("failed to reset migrations: %v\n", err)
			}
			log.Println("All migrations rolled back successfully!")
		} else {
			// Mengubah string versi menjadi int64
			version, err := strconv.ParseInt(*versionFlag, 10, 64)
			if err != nil {
				log.Fatalf("Invalid version format: %v", err)
			}
			// Membatalkan migrasi sampai versi tertentu
			if err := goose.DownTo(db, "db/migrations", version); err != nil {
				log.Fatalf("failed to rollback migrations: %v\n", err)
			}
			log.Printf("Migrations down to version %d rolled back successfully!\n", version)
		}
	default:
		log.Fatalf("Invalid direction. Use 'up' or 'down'.")
	}
}

// goose -dir db/migrations postgres "user=pgusers password=cokro123 dbname=dbtest sslmode=disable host=localhost port=6111" down-to 20240612042344

