module dbtest

go 1.21.5

require (
	github.com/lib/pq v1.10.9 // Driver PostgreSQL untuk Golang
	github.com/pressly/goose/v3 v3.20.0 // Goose untuk migrasi
)

require (
	github.com/mfridman/interpolate v0.0.2 // indirect
	github.com/sethvargo/go-retry v0.2.4 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
)
