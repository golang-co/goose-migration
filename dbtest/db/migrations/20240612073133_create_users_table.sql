-- +goose Up
-- +goose StatementBegin
CREATE TABLE users2 (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE users2;
-- +goose StatementEnd
